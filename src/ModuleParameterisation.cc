#include "ModuleParameterisation.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "G4VPhysicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Box.hh"


ModuleParameterisation::ModuleParameterisation(G4double spacingX, G4double spacingY, G4double barLength, G4int nTotalReplicas, G4double detectorPlaneSide)
{
    fSpacingX =  spacingX;
    fSpacingY =  spacingY;
    fLength   =  barLength;
    fTotal    =  nTotalReplicas;
    detPlaneSide = detectorPlaneSide;

}


ModuleParameterisation::~ModuleParameterisation() 
{;}


void ModuleParameterisation::ComputeTransformation (const G4int copyNo, G4VPhysicalVolume* physVol) const
{
    
    G4int numberOfCrystalsPerMatrix = 5;    // TODO: non dovrebbe essere hardcoded!
    G4int numberOfHolesPerMatrix = 3;    // TODO: non dovrebbe essere hardcoded!

    G4int matrixNumber      = copyNo/numberOfCrystalsPerMatrix;
    G4int matrixNumber_hole = copyNo/numberOfHolesPerMatrix;

    
    G4double xCell = 0.*cm;
    G4double yCell = 0.*cm;
    G4double zCell = 0.*cm;
    
    G4double crystInMatrix = 0;
    G4double holeInMatrix = 0;

    G4double sdd_x = detPlaneSide/4.;
    G4double sdd_y = detPlaneSide/4.;

    G4double voffset1 = 27.6*mm + 1.3*mm;
    G4double voffset2 = 1.7*mm;

    if (fTotal == 60) // Crystals
    {
        // Left column
        if(matrixNumber%2 == 0){sdd_x = -sdd_x;}

        if(matrixNumber == 0 || matrixNumber == 1) {sdd_y = voffset1 + fSpacingY/2 + voffset2;}
        else if(matrixNumber == 2 || matrixNumber == 3) {sdd_y = voffset1 - fSpacingY/2 - voffset2;}
        else if(matrixNumber == 4 || matrixNumber == 5) {sdd_y = +fSpacingY/2. + voffset2;}
        else if(matrixNumber == 6 || matrixNumber == 7) {sdd_y = -fSpacingY/2. - voffset2;}
        else if(matrixNumber == 8 || matrixNumber == 9) {sdd_y = -voffset1 + fSpacingY/2 + voffset2;}
        else if(matrixNumber == 10 || matrixNumber == 11) {sdd_y = -voffset1 - fSpacingY/2 - voffset2;}

        crystInMatrix = copyNo % numberOfCrystalsPerMatrix;
        
        xCell = (crystInMatrix-2)*fSpacingX + sdd_x;
        yCell = sdd_y;
        if (matrixNumber == 4 || matrixNumber == 6) {xCell -= 1.5*mm;}
        if (matrixNumber == 5 || matrixNumber == 7) {xCell += 1.5*mm;}


    }
    else if (fTotal == 36) // Collimator holes
    {
        // Left column
        if(matrixNumber_hole%2 == 0){sdd_x = -sdd_x;}

        if(matrixNumber_hole == 0 || matrixNumber_hole == 1) {sdd_y = detPlaneSide/2. - fSpacingY/2. - voffset1;}
        else if(matrixNumber_hole == 2 || matrixNumber_hole == 3) {sdd_y = 1/4.*detPlaneSide;}
        else if(matrixNumber_hole == 4 || matrixNumber_hole == 5) {sdd_y = +fSpacingY/2. + voffset1;}
        else if(matrixNumber_hole == 6 || matrixNumber_hole == 7) {sdd_y = -fSpacingY/2. - voffset1;}
        else if(matrixNumber_hole == 8 || matrixNumber_hole == 9) {sdd_y = -1/4.*detPlaneSide;}
        else if(matrixNumber_hole == 10 || matrixNumber_hole == 11) {sdd_y = -detPlaneSide/2. + fSpacingY/2. + voffset1;}
        
        holeInMatrix = copyNo % numberOfHolesPerMatrix;
        
        xCell = (holeInMatrix-1)*fSpacingX + sdd_x;
        yCell = sdd_y;
        if (matrixNumber_hole == 4 || matrixNumber_hole == 6) {xCell -= 1.5*mm;}
        if (matrixNumber_hole == 5 || matrixNumber_hole == 7) {xCell += 1.5*mm;}


    }

//    G4cout << "copyNo matrixNumber " << copyNo << " " << matrixNumber <<G4endl;
//    G4cout << "copyNo sdd_x sdd_y " << copyNo << " " << sdd_x/cm << " " << sdd_y/cm <<G4endl;
//    G4cout << "copyNo crystInMatrix " << copyNo << " " << crystInMatrix <<G4endl;
//    G4cout << "copyNo xCell yCell zCell " << copyNo << " " << xCell/cm << " " << yCell/cm << " " << zCell/cm <<G4endl;

    physVol->SetTranslation(G4ThreeVector(xCell, yCell, zCell));
    
}



