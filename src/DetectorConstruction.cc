#include "DetectorConstruction.hh"

#include "SensitiveDetector.hh"

#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"

#include "G4NistManager.hh"
#include "G4RunManager.hh"
#include "G4RegionStore.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4VSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4Polyhedra.hh"
#include "G4Trap.hh"
#include "G4RotationMatrix.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4PVReplica.hh"
#include "G4Transform3D.hh"
#include "G4VPVParameterisation.hh"
#include "G4PVParameterised.hh"
#include "globals.hh"

#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4SDManager.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//#include "G4GDMLParser.hh"

#include <fstream>
#include <vector>
#include <string>

//#include "ConfigFile.hh"

// Constructor 
DetectorConstruction::DetectorConstruction() 
:	experimentalHall_log(0), 
	scint_log(0),
    sdd_log(0),
    optcoupler_log(0),
    pcb_log(0),
	experimentalHall_phys(0),
    scint_phys(0),
    sdd_phys(0),
    optcoupler_phys(0),
    pcb_phys(0),
	defaultMaterial(0)
{
	// Size of the experimental hall
    worldSide         = 100.0 *m;

	// Define the materials
	DefineMaterials();	

	// Define the parameters
	DefineParameters();
		
	// Print parameters
	PrintParameters();
	
;}		



// Destructor 
DetectorConstruction::~DetectorConstruction() { } 	



// Definition of the parameters
void DetectorConstruction::DefineParameters()
{
	SetScintSideX(6.94*mm) ;
	SetScintSideY(12.1*mm) ;
	SetScintSideZ(15.0*mm) ;
    
    SetSDDSideX(6.05*mm) ;
    SetSDDSideY(12.1*mm) ;
    SetSDDSideZ(450*um) ;

    SetOptcouplerSideX(6.94*mm) ;
    SetOptcouplerSideY(12.1*mm) ;
    SetOptcouplerSideZ(1*mm) ;

    SetPCBSideX(50*mm) ;
    SetPCBSideY(50*mm) ;
    SetPCBSideZ(1.6*mm) ;

}


// Definition of the materials
void DetectorConstruction::DefineMaterials()
{ 
	G4double a;			// Atomic mass
	G4double z;			// Atomic number
	G4double density;	// Density
	G4int nel;			// Number of elements in a compound
	G4int natoms;       // Number of atoms in a compound
	G4double fractionmass;
    G4int ncomponents;

	// Elements
	G4Element*  H  = new G4Element("Hydrogen"  , "H" , z = 1. , a =  1.008*g/mole);
//	G4Element*  He = new G4Element("Helium"    , "He", z = 2. , a =  4.003*g/mole);
    G4Element*  C  = new G4Element("Carbon"    , "C" , z = 6. , a =  12.01*g/mole);
    G4Element*  N  = new G4Element("Nitrogen"  , "N" , z = 7. , a =  14.01*g/mole);
    G4Element*  O  = new G4Element("Oxygen"    , "O" , z = 8. , a =  16.00*g/mole);
//  G4Element*  Na = new G4Element("Sodium"    , "Na", z = 11., a =  22.99*g/mole);
	G4Element*  Al = new G4Element("Aluminium" , "Al", z = 13., a =  26.98*g/mole);
	G4Element*  Si = new G4Element("Silicon"   , "Si", z = 14., a =  28.08*g/mole);
//  G4Element*  K  = new G4Element("Potassium" , "K" , z = 19., a =  39.10*g/mole);
//  G4Element*  Ti = new G4Element("Titanium"  , "Ti", z = 22., a =  47.87*g/mole);
//	G4Element*  Cu = new G4Element("Copper"    , "Cu", z = 29., a =  63.55*g/mole);
    G4Element*  Ga = new G4Element("Gallium"   , "Ga", z = 31., a =  69.72*g/mole);
//  G4Element*  Ge = new G4Element("Germanium" , "Ge", z = 32., a =  72.63*g/mole);
//  G4Element*  As = new G4Element("Arsenic"   , "As", z = 33., a =  74.92*g/mole);
    G4Element*  Br = new G4Element("Bromine"   , "Br", z = 35., a =  79.90*g/mole);
//  G4Element*  Mo = new G4Element("Molybdenum", "Mo", z = 42., a =  95.94*g/mole);
//  G4Element*  I  = new G4Element("Iodine"    , "I" , z = 53., a = 126.90*g/mole);
//  G4Element*  Cs = new G4Element("Caesium"   , "Cs", z = 55., a = 132.90*g/mole);
    G4Element*  La = new G4Element("Lanthanium", "La", z = 57., a = 138.90*g/mole);
    G4Element*  Gd = new G4Element("Gadolinium", "Gd", z = 64., a = 157.25*g/mole);
//  G4Element*  Ta = new G4Element("Tantalum"  , "Ta", z = 73., a = 180.94*g/mole);
//  G4Element*  W  = new G4Element("Tungsten"  , "W" , z = 74., a = 183.84*g/mole);
//  G4Element*  Pb = new G4Element("Lead"      , "Pb", z = 82., a = 207.20*g/mole);
//  G4Element*  Bi = new G4Element("Bismuth"   , "Bi", z = 83., a = 208.98*g/mole);

	// Materials 
	// Vacuum
	G4Material* Vacuum = new G4Material("Vacuum", density = 1.e-25*g/cm3, nel = 1);
	Vacuum -> AddElement(H, 100*perCent);

    // NIST
    G4NistManager* man = G4NistManager::Instance();
    G4Material* CsI = man->FindOrBuildMaterial("G4_CESIUM_IODIDE");
    G4Material* BGO = man->FindOrBuildMaterial("G4_BGO");
    G4Material* G4_Si = man->FindOrBuildMaterial("G4_Si");
    G4Material* G4_Ti = man->FindOrBuildMaterial("G4_Ti");
    G4Material* G4_Al = man->FindOrBuildMaterial("G4_Al");
    G4Material* G4_AIR = man->FindOrBuildMaterial("G4_AIR");
    G4Material* G4_STAINLESS_STEEL = man->FindOrBuildMaterial("G4_STAINLESS-STEEL");
    G4Material* G4_BRASS = man->FindOrBuildMaterial("G4_BRASS");
    G4Material* G4_Cu = man->FindOrBuildMaterial("G4_Cu");
    G4Material* G4_Mo = man->FindOrBuildMaterial("G4_Mo");
    G4Material* G4_W = man->FindOrBuildMaterial("G4_W");
    G4Material* G4_Ta = man->FindOrBuildMaterial("G4_Ta");
    G4Material* G4_Pb = man->FindOrBuildMaterial("G4_Pb");
    G4Material* G4_SILICON_DIOXIDE = man->FindOrBuildMaterial("G4_SILICON_DIOXIDE");

    // Effective Aluminium Solid
    G4Material* EffectiveAluminiumSolid = new G4Material("EffectiveAluminiumSolid",
                                                         density = 0.25*2.700*g/cm3, nel = 1);
    EffectiveAluminiumSolid -> AddElement(Al, 100*perCent);

    // GAGG
    G4Material* GAGG = new G4Material("GAGG", density = 6.63 *g/cm3, nel = 4);
    GAGG -> AddElement(Gd, natoms=3);
    GAGG -> AddElement(Al, natoms=2);
    GAGG -> AddElement(Ga, natoms=3);
    GAGG -> AddElement(O,  natoms=12);

    // LaBr3
    G4Material* LaBr3 = new G4Material("LaBr3", density = 5.06 *g/cm3, nel = 2);
    LaBr3 -> AddElement(La, 36.68*perCent);
    LaBr3 -> AddElement(Br, 63.32*perCent);

    // Polymide
    G4Material* Polymide = new G4Material("Polymide", density = 1.43*g/cm3, nel = 4);
    Polymide -> AddElement(C, natoms=22);
    Polymide -> AddElement(H, natoms=10);
    Polymide -> AddElement(N, natoms=2);
    Polymide -> AddElement(O, natoms=5);

    // Silicon Oxide
    G4Material* SiliconOxide = new G4Material("SiliconOxide", density = 2.65*g/cm3, nel = 2);
    SiliconOxide -> AddElement(Si, natoms=1);
    SiliconOxide -> AddElement(O,  natoms=2);

    // Diglycidyl Ether of Bisphenol A (First compound of epoxy resin Epotek 301-1)
    G4Material* Epoxy_1 = new G4Material("Epoxy_1", density = 1.16*g/cm3, nel = 3);
    Epoxy_1 -> AddElement(C, natoms=19);
    Epoxy_1 -> AddElement(H, natoms=20);
    Epoxy_1 -> AddElement(O, natoms=4);
    
    // 1,4-Butanediol Diglycidyl Ether (Second compound of epoxy resin Epotek 301-1)
    G4Material* Epoxy_2 = new G4Material("Epoxy_2", density = 1.10*g/cm3, nel = 3);
    Epoxy_2 -> AddElement(C, natoms=10);
    Epoxy_2 -> AddElement(H, natoms=18);
    Epoxy_2 -> AddElement(O, natoms=4);
    
    // 1,6-Hexanediamine 2,2,4-trimetyl (Third compound of epoxy resin Epotek 301-1)
    G4Material* Epoxy_3 = new G4Material("Epoxy_3", density = 1.16*g/cm3, nel = 3);
    Epoxy_3 -> AddElement(C, natoms=9);
    Epoxy_3 -> AddElement(H, natoms=22);
    Epoxy_3 -> AddElement(N, natoms=2);
    
    // Epoxy resin Epotek 301-1
    G4Material* Epoxy_Resin = new G4Material("Epoxy_Resin", density = 1.19*g/cm3, ncomponents = 3);
    Epoxy_Resin -> AddMaterial(Epoxy_1, fractionmass=56*perCent);
    Epoxy_Resin -> AddMaterial(Epoxy_2, fractionmass=24*perCent);
    Epoxy_Resin -> AddMaterial(Epoxy_3, fractionmass=20*perCent);
    
    // FR4 PCB material
    G4Material* FR4 = new G4Material("FR4", density = 1.8*g/cm3, ncomponents=2);
    FR4 -> AddMaterial(SiliconOxide, fractionmass=60*perCent);
    FR4 -> AddMaterial(Epoxy_Resin,  fractionmass=40*perCent);
    
    // Silicone
    G4Material* Silicone = new G4Material("Silicone", density = 1.08 *g/cm3, nel = 2);
    Silicone -> AddElement(Si, natoms=1);
    Silicone -> AddElement(O,  natoms=2);

	// Default materials of the World: vacuum
	defaultMaterial  = Vacuum;
    
    scintMaterial = GAGG;
    sddMaterial = G4_Si;
    optcouplerMaterial = Silicone;
    pcbMaterial = FR4;

}



// Detector construction
G4VPhysicalVolume* DetectorConstruction::Construct()
{
	// Clean old geometry, if any
	G4GeometryManager::GetInstance()->OpenGeometry();
	G4PhysicalVolumeStore::GetInstance()->Clean();
	G4LogicalVolumeStore::GetInstance()->Clean();
	G4SolidStore::GetInstance()->Clean();


	// 0. Experimental hall (world volume)
	// Solid
	G4Box* experimentalHall_box = new G4Box("experimentalHall_box",
									  		worldSide/2., worldSide/2., worldSide/2.);
	// Logical
	experimentalHall_log  = new G4LogicalVolume(experimentalHall_box,
	                                            defaultMaterial, "experimentalHall_log", 0, 0, 0);
	// Physical
	experimentalHall_phys = new G4PVPlacement(0,
											  G4ThreeVector(0,0,0),
											  experimentalHall_log,
											  "experimentalHall_phys",
											  0,
											  false,
											  0);

    
    // Solid
    G4Box* scint_box = new G4Box("scint_box", scint_x/2., scint_y/2., scint_z/2.);
    // Logical
    scint_log = new G4LogicalVolume(scint_box, scintMaterial, "scint_log", 0, 0, 0);
    // Physical
    scint_phys = new G4PVPlacement(0,
                                    G4ThreeVector(0,0,0),
                                    scint_log,
                                    "scint_phys",
                                    experimentalHall_log,
                                    false,
                                    0);

    // Solid
    G4Box* sdd_box = new G4Box("sdd_box", sdd_x/2., sdd_y/2., sdd_z/2.);
    // Logical
    sdd_log = new G4LogicalVolume(sdd_box, sddMaterial, "sdd_log", 0, 0, 0);
    // Physical
    sdd_phys = new G4PVPlacement(0,
                                    G4ThreeVector(0,0,-scint_z/2-optcoupler_z-sdd_z/2),
                                    sdd_log,
                                    "sdd_phys",
                                    experimentalHall_log,
                                    false,
                                    0);

    // Solid
    G4Box* optcoupler_box = new G4Box("optcoupler_box", optcoupler_x/2., optcoupler_y/2., optcoupler_z/2.);
    // Logical
    optcoupler_log = new G4LogicalVolume(optcoupler_box, optcouplerMaterial, "optcoupler_log", 0, 0, 0);
    // Physical
    optcoupler_phys = new G4PVPlacement(0,
                                    G4ThreeVector(0,0,-scint_z/2-optcoupler_z/2),
                                    optcoupler_log,
                                    "optcoupler_phys",
                                    experimentalHall_log,
                                    false,
                                    0);

    // Solid
    G4Box* pcb_box = new G4Box("pcb_box", pcb_x/2., pcb_y/2., pcb_z/2.);
    // Logical
    pcb_log = new G4LogicalVolume(pcb_box, pcbMaterial, "pcb_log", 0, 0, 0);
    // Physical
    pcb_phys = new G4PVPlacement(0,
                                    G4ThreeVector(0,0,-scint_z/2-optcoupler_z-sdd_z-pcb_z/2.),
                                    pcb_log,
                                    "pcb_phys",
                                    experimentalHall_log,
                                    false,
                                    0);


    
	// Set visualization attributes
	// RGB components
	G4Colour white   (1.0, 1.0, 1.0);
	G4Colour red     (1.0, 0.0, 0.0);
	G4Colour green   (0.0, 1.0, 0.0);
	G4Colour blue    (0.0, 0.0, 1.0);
	G4Colour yellow  (1.0, 1.0, 0.0);
	G4Colour magenta (1.0, 0.0, 1.0);
	G4Colour cyan    (0.0, 1.0, 1.0);

	experimentalHall_log -> SetVisAttributes(G4VisAttributes::Invisible);  

	G4VisAttributes* ScintVisAtt= new G4VisAttributes(yellow);
	scint_log -> SetVisAttributes(ScintVisAtt);

    G4VisAttributes* SDDVisAtt= new G4VisAttributes(magenta);
    sdd_log -> SetVisAttributes(SDDVisAtt);

    G4VisAttributes* OptcouplerVisAtt= new G4VisAttributes(cyan);
    optcoupler_log -> SetVisAttributes(OptcouplerVisAtt);

    G4VisAttributes* PCBVisAtt= new G4VisAttributes(green);
    pcb_log -> SetVisAttributes(PCBVisAtt);

	// The function must return the physical volume of the world
	return experimentalHall_phys;
}


void DetectorConstruction::ConstructSDandField()
{
    // Sensitive volume
    // So far, the detector created above is not yet a real detector: it is just
    // a geometrical object (with some attributes) placed within the world volume.
    // To make the volume a detector, which can record e.g. hits, one must
    // define a sensitive volume associated with it.
    // For this purpose, a SensitiveDetector object is instantiated.
    // SD for the Scint
    

    auto sdman = G4SDManager::GetSDMpointer(); // Mandatory since Geant v. 4.10.03

    // Instantiation of the sensitive detector and readout geometry
    SensitiveDetector* scint_SD  = new SensitiveDetector("SCI");
    SensitiveDetector* sdd_SD  = new SensitiveDetector("SDD");


    sdman->AddNewDetector(scint_SD); // Mandatory since Geant v. 4.10.03
    sdman->AddNewDetector(sdd_SD); // Mandatory since Geant v. 4.10.03

    SetSensitiveDetector(scint_log, scint_SD);
    SetSensitiveDetector(sdd_log, sdd_SD);

    
}


void DetectorConstruction::SetScintMaterial(G4String materialChoice)
{
	G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);     
	if (pttoMaterial) scintMaterial = pttoMaterial;
}

void DetectorConstruction::SetSDDMaterial(G4String materialChoice)
{
    G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
    if (pttoMaterial) sddMaterial = pttoMaterial;
}


void DetectorConstruction::SetOptcouplerMaterial(G4String materialChoice)
{
    G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
    if (pttoMaterial) optcouplerMaterial = pttoMaterial;
}


void DetectorConstruction::SetPCBMaterial(G4String materialChoice)
{
    G4Material* pttoMaterial = G4Material::GetMaterial(materialChoice);
    if (pttoMaterial) pcbMaterial = pttoMaterial;
}






void DetectorConstruction::PrintParameters()
{
	G4cout  << "\n-----------------------------------------------------------------------------------\n"
			<< "          Geometrical and physical parameters: "
			<< "\n Scintillator Side X            (cm) : " 	<< scint_x/cm
			<< "\n Scintillator Side Y            (cm) : " 	<< scint_y/cm
			<< "\n Scintillator Side Z            (cm) : " 	<< scint_z/cm
            << "\n Scintillator material               : " 	<< scintMaterial -> GetName()
            << "\n SDD Side X                     (cm) : "  << sdd_x/cm
            << "\n SDD Side Y                     (cm) : "  << sdd_y/cm
            << "\n SDD Side Z                     (cm) : "  << sdd_z/cm
            << "\n SDD material                        : "  << sddMaterial -> GetName()
            << "\n Optical coupler Side X         (cm) : "  << optcoupler_x/cm
            << "\n Optical coupler Side Y         (cm) : "  << optcoupler_y/cm
            << "\n Optical coupler Side Z         (cm) : "  << optcoupler_z/cm
            << "\n Optical coupler material            : "  << optcouplerMaterial -> GetName()
            << "\n PCB Side X                     (cm) : "  << pcb_x/cm
            << "\n PCB Side Y                     (cm) : "  << pcb_y/cm
            << "\n PCB Side Z                     (cm) : "  << pcb_z/cm
            << "\n PCB material                        : "  << pcbMaterial -> GetName();
	G4cout  << "\n-----------------------------------------------------------------------------------\n";
}



void DetectorConstruction::UpdateGeometry()
{  
	G4RunManager::GetRunManager()->DefineWorldVolume(Construct());
	G4RunManager::GetRunManager()->GeometryHasBeenModified();
}


