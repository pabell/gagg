#ifndef DetectorConstruction_H
#define DetectorConstruction_H 1

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class SensitiveDetector;
class G4VPVParameterisation;

#include "G4VUserDetectorConstruction.hh"

// Mandatory user class that defines the detector used in the
// simulation, its geometry and its materials.
// Derived from the G4VUserDetectorConstruction initialisation 
// abstract base class.

class DetectorConstruction : public G4VUserDetectorConstruction
{
	public:
		DetectorConstruction();		// Constructor
		~DetectorConstruction();	// Destructor

	private:
        // Method to construct the detector
        G4VPhysicalVolume* Construct();
        // Method to construct the sensitive detector
        void ConstructSDandField();
        // Method to define the materials
        void DefineMaterials();
        // Method to read the values of parameters from file
        void DefineParameters();

	public:
		// Get methods
		// Method to get the world physical volume
	    const G4VPhysicalVolume* GetWorld()     {return experimentalHall_phys;};

	    // Method to get the world dimensions
		G4double GetWorldSide()               	{return worldSide;};

		// Method to get the scintillator physical volume
	    const G4VPhysicalVolume* GetScint()  	{return scint_phys;};

	    // Methods to get the scintillator bar dimensions
		G4double GetScintSideX()          		{return scint_x;};
		G4double GetScintSideY()          		{return scint_y;};
		G4double GetScintSideZ()          		{return scint_z;};

        // Methods to get the materials
        G4Material* GetScintMaterial()        	{return scintMaterial;};
        G4Material* GetSDDMaterial()            {return sddMaterial;};
        G4Material* GetOptcouplerMaterial()     {return optcouplerMaterial;};
        G4Material* GetPCBMaterial()            {return pcbMaterial;};

		// Set methods
		// Method to set the world dimensions
		void SetWorldSide(G4double wside)		    {worldSide = wside;};

		// Methods to set the scintillator bar dimensions
		void SetScintSideX(G4double scintsidex)		{scint_x = scintsidex;};
		void SetScintSideY(G4double scintsidey)		{scint_y = scintsidey;};
		void SetScintSideZ(G4double scintsidez)		{scint_z = scintsidez;};
		
        // Methods to set the SDD dimensions
        void SetSDDSideX(G4double sddsidex)        {sdd_x = sddsidex;};
        void SetSDDSideY(G4double sddsidey)        {sdd_y = sddsidey;};
        void SetSDDSideZ(G4double sddsidez)        {sdd_z = sddsidez;};
        
        // Methods to set the optical coupler dimensions
        void SetOptcouplerSideX(G4double optcouplersidex)   {optcoupler_x = optcouplersidex;};
        void SetOptcouplerSideY(G4double optcouplersidey)   {optcoupler_y = optcouplersidey;};
        void SetOptcouplerSideZ(G4double optcouplersidez)   {optcoupler_z = optcouplersidez;};

        // Methods to set the PCB dimensions
        void SetPCBSideX(G4double pcbsidex)        {pcb_x = pcbsidex;};
        void SetPCBSideY(G4double pcbsidey)        {pcb_y = pcbsidey;};
        void SetPCBSideZ(G4double pcbsidez)        {pcb_z = pcbsidez;};


		// Methods to set the materials
		void SetScintMaterial           (G4String);
        void SetSDDMaterial             (G4String);
        void SetOptcouplerMaterial      (G4String);
        void SetPCBMaterial             (G4String);

	    // Print a list of parameters
		void PrintParameters();
	
		// Geometry update
		void UpdateGeometry();


	private:
		// Logical volumes
		G4LogicalVolume* experimentalHall_log;
        G4LogicalVolume* scint_log;
        G4LogicalVolume* sdd_log;
        G4LogicalVolume* optcoupler_log;
        G4LogicalVolume* pcb_log;


		// Physical volumes
		G4VPhysicalVolume* experimentalHall_phys;
		G4VPhysicalVolume* scint_phys;
        G4VPhysicalVolume* sdd_phys;
        G4VPhysicalVolume* optcoupler_phys;
        G4VPhysicalVolume* pcb_phys;

		// World dimensions
		G4double worldSide;

		// Scint dimensions 
		G4double scint_x;
		G4double scint_y;
		G4double scint_z;
    
        // SDD dimensions
        G4double sdd_x;
        G4double sdd_y;
        G4double sdd_z;
    
        // Optical coupler dimensions
        G4double optcoupler_x;
        G4double optcoupler_y;
        G4double optcoupler_z;
    
        // PCB dimensions
        G4double pcb_x;
        G4double pcb_y;
        G4double pcb_z;
		
		// Materials
		G4Material* defaultMaterial;
		G4Material*	scintMaterial;
        G4Material* sddMaterial;
        G4Material* optcouplerMaterial;
        G4Material* pcbMaterial;

};

#endif

